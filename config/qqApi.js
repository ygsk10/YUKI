SESSION_URL = "https://fanyi.qq.com/"
AUTH_URL = "https://fanyi.qq.com/api/reauth12f"
TRANSLATE_URL =
  "https://fanyi.qq.com/api/translate"
USER_AGENT =
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"

var session
var bv
var requestTranslation
var initSession
var qtv
var qtk

if (!session) {
  session = Request.jar()

  requestTranslation = () => {
    return new Promise((resolve, reject) => {
      Request.post(TRANSLATE_URL, {
        jar: session,
        gzip: true,
        headers: {
          Referer: SESSION_URL,
          "User-Agent": USER_AGENT
        },
        form: {
          source: "jp",
          target: "zh",
          sourceText: text,
          qtv: qtv,
          qtk: qtk
        }
      })
        .then(body => {
          let sentences = JSON.parse(body).translate.records
          let result = "";
          for (let i in sentences) {
            result += sentences[i].targetText
          }
          result = result.replace(/({[^}]*})|(\(\([^\)]*\)\))/g, '')
          if (result === '') initSession()
          else callback(result)
        })
        .catch(err => {
          callback(qtv, qtk)
        });
    });
  };

  initSession = () => {
    return Request.post(AUTH_URL, {
      json: true,
      headers: {
        Referer: SESSION_URL,
        Origin: SESSION_URL,
        "User-Agent": USER_AGENT
      }
    })
      .then(body => {
        qtv = body.qtv
        qtk = body.qtk
      })
      .then(requestTranslation)
  };

  initSession()
} else {
  requestTranslation()
}
